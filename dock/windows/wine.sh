#!/bin/sh

if [ -z $DISPLAY ]; then
    Xvfb -ac :3 -screen 0 1280x1024x24 &
    export DISPLAY_PID=$!
    export DISPLAY=":3"
fi

python_version() {
    local url='https://www.python.org/ftp/python/'

    curl -s "$url" |
        sed -n 's!.*href="\([0-9]\+\.[0-9]\+\.[0-9]\+\)/".*!\1!p' |
        sort -rV |
    while read -r version; do
        filename="Python-$version.tar.xz"
        # Versions which only have alpha, beta, or rc releases will fail here.
        # Stop when we find one with a final release.
        if curl --fail --silent -O "$url/$version/$filename"; then
            echo "$version"
            break
        fi
    done
}

upx_version() {
    local url='https://api.github.com/repos/upx/upx/releases/latest'

    curl -s "$url" |
    grep -i "\"tag_name\"" |
    cut -d '"' -f 4
}

create_prefix() {
    export WINEPREFIX=/wine
    export WINEDEBUG="-all"
    export WINEDLLOVERRIDES="winemenubuilder.exe,mscoree,mshtml="
    export WINEPATH='C:\Python310\Scripts'
    export PYTHON_VERSION=3.11.0 #$(python_version)
    export UPX_VERSION=$(upx_version)
    xvfb-run sh -c "wine reg add 'HKLM\Software\Microsoft\Windows NT\CurrentVersion' /v CurrentVersion /d 10.0 /f && \
    wine reg add 'HKCU\Software\Wine\DllOverrides' /v winemenubuilder.exe /t REG_SZ /d '' /f && \
    wine reg add 'HKCU\Software\Wine\DllOverrides' /v mscoree /t REG_SZ /d '' /f && \
    wine reg add 'HKCU\Software\Wine\DllOverrides' /v mshtml /t REG_SZ /d '' /f; \
    wineserver -w"
    curl https://www.python.org/ftp/python/${PYTHON_VERSION}/python-${PYTHON_VERSION}-amd64.exe -o /tmp/python-${PYTHON_VERSION}-amd64.exe
    curl https://github.com/upx/upx/releases/download/v${UPX_VERSION}/upx-${UPX_VERSION}-win64.zip -o /tmp/upx-${UPX_VERSION}-win64.zip

    xvfb-run sh -c "\
        wine /tmp/python-${PYTHON_VERSION}-amd64.exe /quiet TargetDir=C:\\Python310 \
        Include_doc=0 InstallAllUsers=1 PrependPath=1; \
        wineserver -w"

    cd /tmp/

    unzip -o upx*.zip && \
    mv -v upx*/upx.exe ${WINEPREFIX}/drive_c/windows/

    rm -rf upx* python*.exe
    wine python -m pip install --upgrade setuptools wheel pip
    wine python -m pip install pyinstaller
}

create_prefix

if ! [ -z $DISPLAY_PID ]; then
    kill -9 $DISPLAY_PID
    unset DISPLAY_PID
fi
