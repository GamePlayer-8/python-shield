#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

for X in $(find "$SCRIPT_PATH/dock" -type d -name 'app'); do
    rm -rf "$X"
done
