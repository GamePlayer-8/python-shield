#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"

cp -r app dock/alpine/
cp -r app dock/ubuntu/
cp -r app dock/windows/
