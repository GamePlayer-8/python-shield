<p align="center" style="white-space: pre-line;">
  <a href="https://pyshield.chimmie.k.vu" class="no-highlight">
    <img src="res/python-shield.png" width="400" alt=":python-shield-splash:">
  </a>
</p>

<p align="center">
  <a href="https://ci.codeberg.org/repos/12783"><img src="https://ci.codeberg.org/api/badges/12783/status.svg" alt=":workflow:" height="20" /></a>
  <a href="https://codeberg.org/GamePlayer-8/python-shield"><img alt=":give_a_star:" src="https://img.shields.io/badge/Give_a-Star_⭐-green" height="20" /></a>

</p>

Python Shield is an action for Woodpecker CI.<br/>
Allows packaging your Python app into a standalone application for Windows & Linux.

# Usage

## With containers

### Building for Windows

Run `podman run --rm -it codeberg.org/gameplayer-8/python-shield:latest-win pyshield help` to get into help menu.

### Building for GlibC (standard Linux systems)

Run `podman run --rm -it codeberg.org/gameplayer-8/python-shield:latest-ubuntu pyshield help` to get into help menu.

### Building for Musl (Alpine systems etc)

Run `podman run --rm -it codeberg.org/gameplayer-8/python-shield:latest pyshield help` to get into help menu.

# License

[MIT](LICENSE.txt)
